import pefile
import re


INPATH = r'atieclxx.exe'
OUTPATH = r'atieclxx.exe.patched'


# https://stackoverflow.com/a/4665027
def find_all(a_str, sub):
	start = 0
	while True:
		start = a_str.find(sub, start)
		if start == -1: return
		yield start
		start += len(sub)

def funs_by_addr(pe):
	return {x.address: x.name for y in pe.DIRECTORY_ENTRY_IMPORT for x in y.imports}

def find_calls(pe, targets=None):
	funs = funs_by_addr(pe)
	for offset in find_all(pe.__data__, b'\xff\x15'):
		rva = pe.get_rva_from_offset(offset)
		operand = int.from_bytes(pe.get_data(rva+2, 4), 'little')
		t = pe.OPTIONAL_HEADER.ImageBase + rva + 6 + operand
		if t in funs:
			t = funs[t]
		if (not targets) or (t in targets):
			yield (offset, t)


nops = []
pe = pefile.PE(INPATH)
calls = list(find_calls(pe, [b'RegisterRawInputDevices', b'GetRawInputData', b'QueryPerformanceCounter']))

for o, name in calls:
	if name != b'RegisterRawInputDevices':
		continue
	regex = (
		b'\xff\x15....'  # Starts with a call to RegisterRawInputDevices
		b'\x85\xc0\x75.\x48......\xeb.'  # Sets up to call the loop function
	)
	if re.match(regex, pe.__data__[o:o+19]):
		nops.append((o,19))
		print(f'Patching "RegisterRawInputDevices" call at {hex(o)}')
	else:
		print(f'Couldn\'t patch "RegisterRawInputDevices" call at {hex(o)}')

for i in range(len(calls)-1):
	a = calls[i]
	b = calls[i+1]
	if a[1] == b'GetRawInputData' and b[1] == b'QueryPerformanceCounter':
		regex = (
			b'(\xff\x15.*)'  # Starts with a call to GetRawInputData
			b'.\x8d......'  # LEA to set up...
			b'\xff\x15'  # ... for a call to QueryPerformanceCounter
		)
		m = re.match(regex, pe.__data__[a[0]:b[0]+2])
		if m:
			nops.append((a[0],len(m.group(1))))
			print(f'Patching "GetRawInputData" call at {hex(a[0])}')
		else:
			print(f'Couldn\'t patch "GetRawInputData" call at {hex(a[0])}')

for o, n in nops:
	pe.set_data_bytes(o, b'\x90'*n)

pe.write(OUTPATH)
print(f'Wrote "{OUTPATH}"')
